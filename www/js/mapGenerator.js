var columns = 16,
	rows = 16,
	tileMap,
	potentialGoalTiles = [],
	goalList = [];


function generateMap() {
	initMapTiles();

	generateActualGameWalls();


	//// console.log(JSON.stringify(tileMap));
}

function generateActualGameWalls() {
	addWall(4, 0, "right");
	addWall(9, 0, "right");
	addWall(13, 0, "bottom");

	addWall(2, 1, "right");
	addWall(2, 1, "bottom");
	addToGoalTilesList(2, 1);
	addWall(13, 1, "right");
	addToGoalTilesList(13, 1);

	addWall(9, 2, "bottom");

	addWall(0, 3, "right");
	addWall(1, 3, "bottom");
	addToGoalTilesList(1, 3);
	addWall(6, 3, "bottom");
	addWall(8, 3, "right");
	addToGoalTilesList(9, 3);

	addWall(0, 4, "bottom");
	addWall(5, 4, "right");
	addToGoalTilesList(6, 4);
	addWall(15, 4, "bottom");

	addWall(5, 5, "bottom");

	addWall(5, 6, "right");
	addToGoalTilesList(5, 6);
	addWall(7, 6, "bottom");
	addWall(8, 6, "bottom");
	addWall(10, 6, "right");
	addToGoalTilesList(10, 6);
	addWall(10, 6, "bottom");
	addWall(13, 6, "right");
	addWall(14, 6, "bottom");
	addToGoalTilesList(14, 6);

	addWall(3, 7, "right");
	addToGoalTilesList(3, 7);
	addWall(3, 7, "bottom");
	addWall(6, 7, "right");
	addWall(8, 7, "right");

	addWall(2, 8, "bottom");
	addWall(4, 8, "right");
	addWall(5, 8, "bottom");
	addToGoalTilesList(5, 8);
	addWall(6, 8, "right");
	addWall(7, 8, "bottom");
	addWall(8, 8, "bottom");
	addWall(8, 8, "right");
	addWall(14, 8, "bottom");

	addWall(1, 9, "right");
	addToGoalTilesList(2, 9);
	addWall(13, 9, "right");
	addToGoalTilesList(14, 9);

	addWall(10, 10, "right");
	addWall(11, 10, "bottom");
	addToGoalTilesList(11, 10);
	addWall(15, 10, "bottom");

	addWall(0, 11, "bottom");
	addWall(9, 11, "bottom");

	addWall(4, 12, "bottom");
	addWall(9, 12, "right");
	addToGoalTilesList(9, 12);

	addWall(4, 13, "right");
	addToGoalTilesList(4, 13);

	addWall(1, 14, "right");
	addToGoalTilesList(1, 14);
	addWall(1, 14, "bottom");
	addWall(13, 14, "right");
	addToGoalTilesList(13, 14);
	addWall(13, 14, "bottom");

	addWall(5, 15, "right");
	addWall(10, 15, "right");
}

function addToGoalTilesList(xIndex, yIndex) {
	tileMap[xIndex][yIndex].goal = "goal";

}

function generateRandomWalls(wallsPerRow) {
	for (var i = 0; i < 16; i++) {
		for (var j = 0; j < 3; j++) {
			var randomNumber = Math.floor(Math.random() * 16);
			var randomDirection = Math.floor((Math.random() * 4));

			if (randomDirection === 0) {
				addWall(i, randomNumber, "top");
			} else if (randomDirection == 1) {
				addWall(i, randomNumber, "right");
			} else if (randomDirection == 2) {
				addWall(i, randomNumber, "bottom");
			} else {
				addWall(i, randomNumber, "left");
			}

		}
	}
}

function getRandom16() {
	return Math.floor((Math.random() * 16));
}

function getRandomEmptySpot() {
	randomX = Math.floor((Math.random() * 16));
	randomY = Math.floor((Math.random() * 16));

	if ((randomX == 7 || randomX == 8) && (randomY == 7 || randomY == 8)) {
		getRandomEmptySpot();
	}
	for (var i = 0; i < penguinList.length; i++) {
		if (penguinList[i].x == randomX && penguinList[i].y == randomY) {
			getRandomEmptySpot();
		}
	}
	if (map.tileMap[randomX][randomY].goal !== undefined) {
		getRandomEmptySpot();
	}

	returnArray = [randomX, randomY];
	return returnArray;
}

/**
 * Initializes all the maps tiles and gives them BASIC walls.
 * @return {[type]}
 */
function initMapTiles() {
	tileMap = [];
	for (var i = 0; i < rows; i++) {
		tileMap[i] = [];
		for (var j = 0; j < columns; j++) {
			tileMap[i][j] = new Tile(false, false, false, false);

			if (j === 0) {
				tileMap[i][j].wallTop = true;
			} if (i === 0) {
				tileMap[i][j].wallLeft = true;
			} if (j === columns - 1) {
				tileMap[i][j].wallBottom = true;
			} if (i === rows - 1) {
				tileMap[i][j].wallRight = true;
			}
		}
	}
}

/**
 * used to add individual walls
 * not sure if is useful in the end?
 * wall direction 0,1,2,3 > up,right,down,left
 * for test purposes now
 * @param	{int} xIndex
 * @param	{int} yIndex
 * @param	{String} wallDirection
 */
function addWall(xIndex, yIndex, wallDirection) {
	if ((xIndex === 0 && wallDirection == "left") ||
		(xIndex == columns - 1 && wallDirection == "right") ||
		(yIndex === 0 && wallDirection == "top") ||
		(yIndex == rows - 1 && wallDirection == "bottom")) {
		wallDirection = "";
	}
	// UP
	if (wallDirection == "top") {
		tileMap[xIndex][yIndex].wallTop = true;
		tileMap[xIndex][yIndex - 1].wallBottom = true;
	}
	// RIGHT
	else if (wallDirection == "right") {
		tileMap[xIndex][yIndex].wallRight = true;
		tileMap[xIndex + 1][yIndex].wallLeft = true;
	}
	// DOWN
	else if (wallDirection == "bottom") {
		tileMap[xIndex][yIndex].wallBottom = true;
		tileMap[xIndex][yIndex + 1].wallTop = true;
	}
	// LEFT
	else if (wallDirection == "left") {
		tileMap[xIndex][yIndex].wallLeft = true;
		tileMap[xIndex - 1][yIndex].wallRight = true;
	}
}

/**
 * Adds a goal to the specified tile in the tileMap.
 * @param {int} xIndex
 * @param {int} yIndex
 * @param {Goal} goal
function addGoal(xIndex, yIndex, goal) {
	tileMap[xIndex][yIndex].goal = goal;
}
*/
