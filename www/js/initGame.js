// Declare global variables
var backgroundCanvas,
	backgroundContext,
	penguinCanvas,
	penguinContext,
	hudCanvas,
	hudContext,
	penguinList,
	redPenguin,
	bluePenguin,
	greenPenguin,
	yellowPenguin,
	silverPenguin,
	currentPenguin,
	map,
	timeInterval;

/**
 * Initialize the contexts and map and starts the whole game.
 */
function init() {
	// Generate the contexts and hide the canvases for now
	backgroundContext = initContext("background-layer");
	penguinContext = initContext("penguin-layer");
	hudContext = initContext("hud-layer");

	// Use if you want to create JSON of the map
	// generateMap();
	//// console.log(JSON.stringify(map.tileMap));
	// Initialize the map from the JSON
	loadMap(goalMap1);

	// Setup controls for touch devices
	initiateSwipeListener();

	gameState = "menu";
}

/**
 * Create a new game with random goals and penguin positions.
 */
function createNewGame() {
	gameState = "game";

	map.randomizeGoals();
	map.newRandomGoal();

	penguinContext.clearRect(0, 0, 720, 1280);
	map.clearPenguins();
	initPenguins();
	for (var i = 0; i < penguinList.length; i++) {
		penguinList[i].drawPenguin();
	}
	// The default penguin is the red one
	currentPenguin = redPenguin;
	// draw the map and penguin
	map.drawMap();

	// every time a new game is created, reset wincount
	winCount = 0;
}

/**
 * Setup the global context-variables by taking the canvas from the html-document
 * @param  {string} canvasId
 * @return {context}
 */
function initContext(canvasId) {
	canvas = document.getElementById(canvasId);
	// Set it up as a HD-canvas
	var windowWidth = 720;
	var windowHeight = 1280;
	canvas.width = windowWidth;
	canvas.height = windowHeight;
	// Set context to 2d and then return it
	context = canvas.getContext('2d');
	return context;
}

/**
 * Creates 5 different Penguins in random available spots on the map
 * and stores them in the penguinList.
 * @return {[type]}
 */
function initPenguins() {
	// console.log("penguin list before init: " + penguinList);
	penguinList = [];
	redPenguin = new Penguin(getRandomEmptySpot(), "red", document.getElementById("redBoardPingu"));
	penguinList.push(redPenguin);
	bluePenguin = new Penguin(getRandomEmptySpot(), "blue", document.getElementById("blueBoardPingu"));
	penguinList.push(bluePenguin);
	greenPenguin = new Penguin(getRandomEmptySpot(), "green", document.getElementById("greenBoardPingu"));
	penguinList.push(greenPenguin);
	yellowPenguin = new Penguin(getRandomEmptySpot(), "yellow", document.getElementById("yellowBoardPingu"));
	penguinList.push(yellowPenguin);
	silverPenguin = new Penguin(getRandomEmptySpot(), "silver", document.getElementById("silverBoardPingu"));
	penguinList.push(silverPenguin);
	// console.log("penguin list after init: " + penguinList);
}

/**
 * An universal drawer
 * @param  {object} object
 * @param  {string} color
 */
function draw(object, color) {
	context.fillStyle = color;
	context.fillRect(object.x, object.y, object.width, object.height);
	// console.log("drew color " + color);
}

/**
 * Load the map from the JSON, assign it to the global Map -object
 * and create the tiles from the JSOn to populate the Map -objects TileMap.
 * @param  {String} levelName
 */
function loadMap(levelName) {
	var mapObject = JSON.parse(levelName);
	map = new Map();
	map.createMapTiles(mapObject);
}

/**
 * Use hammer for recognising swipes
 */
function initiateSwipeListener() {
	// Setup hammer to listen to the backgroundcanvas-element
	var hammer = new Hammer(document.getElementById("hud-layer"));
	// enable up and down swipe too
	hammer.get("swipe").set({ direction: Hammer.DIRECTION_ALL});
	// Recognise all the different swipes and act as needed
	hammer.on("swipeup", function() {
		currentPenguin.movePenguin("up");
	});
	hammer.on("swiperight", function() {
		currentPenguin.movePenguin("right");
	});
	hammer.on("swipedown", function() {
		currentPenguin.movePenguin("down");
	});
	hammer.on("swipeleft", function() {
		currentPenguin.movePenguin("left");
	});
}

// Start the party!
init();
