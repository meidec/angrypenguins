var buttonList;

/**
 * Selects the currentPenguin when the user presses the corresponding button.
 * @param  {String} color
 */
function selectPenguin(color) {
	//playAudio("penguinSound");

	// disable all the buttons if a penguin is moving on board
	if (!isAnimationInProgress) {
	// // console.log("Switched to " + color);
		switch (color) {
			case "red":
				currentPenguin = redPenguin;
				break;
			case "blue":
				currentPenguin = bluePenguin;
				break;
			case "green":
				currentPenguin = greenPenguin;
				break;
			case "yellow":
				currentPenguin = yellowPenguin;
				break;
			case "silver":
				currentPenguin = silverPenguin;
				break;
			default:
				break;
		}
	}
}

// TODO check if works correctly
/**
 * Starts a new game for the palyer to play.
 * Shows all the canvases and penguinButtons.
 */
function startGame() {
    // Hide the main menu screen
    document.getElementById('menu-container').style.display = "none";
    // Show the real game and the canvases
    document.getElementById('background-layer').style.display = "block";
    document.getElementById('penguin-layer').style.display = "block";
    document.getElementById('hud-layer').style.display = "block";
	// Draw the background for the game screen
	var backgroundImage = document.getElementById('game-background');
	backgroundContext.drawImage(backgroundImage, 0, 0);

	// Make a list containing all penguinButtons
	buttonList = [
		document.getElementById('redbutton'),
		document.getElementById('bluebutton'),
		document.getElementById('greenbutton'),
		document.getElementById('yellowbutton'),
		document.getElementById('silverbutton')
	];
	disableButtons(false);
	// // console.log(buttonList);

    // Show the buttons for Penguins
    document.getElementById('penguin-container').style.display = "inline";
	// Start the timer.
    timeInterval = startTimer(TURNTIME);

	createNewGame();
}

function backPressed() {
	// // console.log("back pressed");
	// if game is being played and back is pressed, open pause menu
	if (gameState == "game") {
		pauseGame(true);
	// if game is paused and back is pressed, close the pause menu
	} else if (gameState == "paused") {
		pauseGame(false);
	// if menu is opened and back is pressed, close the application
	} else if (gameState == "menu") {
		// // console.log("exit app!!!");
		//if (navigator.app) {
			// console.log("found app");
			navigator.app.exitApp();
		//} else if (navigator.device) {
		//	// console.log("found device");
			navigator.device.exitApp();
		//}
	}
}

/**
 * this is used to create a popupwindow
 * there are three different windows: when back button is pressed and when game is over and when game is won
 */
function showPopupMenu(isVisible) {
	document.getElementById('game-popup').style.display = (isVisible?'inline':'none');

	var popupPenguin1 = document.getElementById('popup-penguin1')
	var popupPenguin2 = document.getElementById('popup-penguin2')
	var informationString = "";
	switch(gameState) {
    case "paused":
        informationString = "Game Paused";
        popupPenguin1.src = "res/textures/redPingu.svg"
        popupPenguin2.src = "res/textures/redPingu.svg"
        break;
    case "win":
    	informationString = "You Win!!!";
    	popupPenguin1.src = "res/textures/penguins/happypenguin.png"
        popupPenguin2.src = "res/textures/penguins/happypenguin.png"
    	break;
    case "lose":
    	informationString = "Game Over :(";
    	popupPenguin1.src = "res/textures/penguins/sadpenguin.png"
        popupPenguin2.src = "res/textures/penguins/sadpenguin.png"
    	break;
    default:
        informationString = "whoops this message should not come up";
}
	document.getElementById('popup-information').innerHTML = informationString;
	disableButtons(isVisible, isVisible);
	dimBackground(isVisible);
}

/**	is used when back button is pressed during a game
 *  freezes time and shows popupscreen
 *
 *
 */
function pauseGame(isPaused) {
	if (isPaused) {
		gameState = "paused";
		// stop the timer
		window.clearInterval(timeInterval);
	} else {
		gameState = "game";
		// continue the timer
		timeInterval = startTimer(timeLeft);
	}
	showPopupMenu(isPaused);


}

/** dim the background so that popup-menu is more visible
 * @param {boolean} isDimmed
 */
function dimBackground(isDimmed) {
	document.getElementById('background-dimmer').style.display = (isDimmed?'block':'none')
}

/**
 * this function is called during penguin animations
 * is used to disable/enable the buttons while penguin moves from A to B
 */
function disableButtons(buttonDisabled, showButtons) {
	for (var i = 0; i < buttonList.length; i++) {
		buttonList[i].disabled = buttonDisabled;
		buttonList[i].style.display = (!showButtons?'inline-block':'none');
	}
}

/**
 * Hides the popup, resets the timer and creates a new game.
 */
function restartGame() {
	showPopupMenu(false);
	timeInterval = startTimer(TURNTIME);
	createNewGame();
}

// TODO check if works correctly
/**
 * Exits the current game and returns to the main menu.
 */
function exitGame() {
	// Undim background
	dimBackground(false);

	gameState = "menu";

	showPopupMenu(false);

    // Hide the real game and the canvases
    document.getElementById('background-layer').style.display = "none";
    document.getElementById('penguin-layer').style.display = "none";
    document.getElementById('hud-layer').style.display = "none";
    // Hide the buttons for Penguins
    document.getElementById('penguin-container').style.display = "none";

	// Show the main menu screen
    document.getElementById('menu-container').style.display = "block";
}

/**
 * Draws the timer to the hudCanvas as the Timer updates.
 * @param  {[type]} paramTime
 */
function drawTimer(paramTime) {
	hudContext.clearRect(8 * TILESIZE + 5, 8 * TILESIZE + 5, 2 * TILESIZE - 10, 2 * TILESIZE - 10);

	hudContext.font = "50px Verdana";
	hudContext.fillText(paramTime, 8.3 * TILESIZE, 9.5 * TILESIZE);
}
