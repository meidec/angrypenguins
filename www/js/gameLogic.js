var timeLeft,
	// Defines the length of the turn.
	TURNTIME = 60,
	currentTurnTime,
	winCount = 0,
	// how many wins are needed to win the game, max 17?
	WINCONDITION = 8,

	/** POSSIBLE GAME STATES:::
	 *  menu
	 *  game
	 *	paused
	 *  win
	 *  lose
	 */
	gameState;

/**
 * Checks if a winning move has happened, meaning:
 * the correct penguin in the correct goal.
 * @return {[type]}
 */
function checkWinner() {
	var penguinGoal = map.tileMap[currentPenguin.x][currentPenguin.y].goal;
	if (penguinGoal === map.currentGoal) {
		if (currentPenguin.color === map.currentGoal.color) {
			handleWin();
		} else if (map.currentGoal.color === "rainbow") {
			handleWin();
		}
	}
}

// TODO Victory and points etc
/**
 * Gets called when victory happens.
 * Increases the winCount -variable and creates a new goal
 * for the player. Also starts the timer again.
 */
function handleWin() {
	// console.log("WINWINWIN");
	winCount++;
	// console.log("Wins so far: " + winCount);

	window.clearInterval(timeInterval);
	if (winCount >= WINCONDITION) {
		// // console.log("you won the whole game½!!!");
		gameState = "win";
		showPopupMenu(true);
	} else {
	map.newRandomGoal();
	map.drawMap();
	timeInterval = startTimer(TURNTIME);
	}
}

/**
 * Get's called when the player loses(time runs out).
 * Clears the timer, shows the game over popup,
 * clears the penguinContext and penguins from the map.
 */
function handleLose() {
	// // console.log("loser =(");
	window.clearInterval(timeInterval);

	gameState = "lose";
	showPopupMenu(true);
}

/**
 * Used to initialize the timer.
 * @param  {[int]} timeRemaining
 * @return {[function]}
 */
function createCountDown(timeRemaining) {
    var startTime = Date.now();
    return function() {
       return timeRemaining - ( Date.now() - startTime );
   };
}

/**
 * Starts a timer using 1sec Intervals.
 * @param  {[int]} time
 * @return {[function]}
 */
function startTimer(time) {
	currentTurnTime = time;
	var currentCountDown = createCountDown(time);
	drawTimer(time);
	return window.setInterval(function() {
		updateTimer(currentCountDown);
	}, 1000);
}

/**
 * Updates the current time of the Timer and calls for
 * the drawTimer.
 * @param  {int} currentCountDown
 */
function updateTimer(currentCountDown) {
	timeLeft = currentTurnTime + Math.floor(currentCountDown() / 1000);
	if (timeLeft <= 0) {
		handleLose();
	}
	drawTimer(timeLeft);
}
