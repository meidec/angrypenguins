// universal tile size
// wallSize determines how thick walls are drawn
var TILESIZE = 40,
	WALLSIZE = TILESIZE / 5;

/**
 * A single Tile hold all the information a single cell in the matrix has.
 * @param  {boolean} wallTop
 * @param  {boolean} wallBottom
 * @param  {boolean} wallLeft
 * @param  {boolean} wallRight
 */
var Tile = function(wallTop, wallBottom, wallLeft, wallRight) {
	this.wallTop = wallTop;
	this.wallBottom = wallBottom;
	this.wallLeft = wallLeft;
	this.wallRight = wallRight;

	this.hasPenguin = false;
	this.goal = undefined;
	this.texture = undefined;

	/**
	 * Draw the tile: first just draw a simple rectangle, then add walls if the tile has some
	 * @param	{context} context
	 * @param	{number} tileX
	 * @param	{number} tileY
	 */
	 this.drawTile = function(tileX, tileY) {


		if (this.goal !== undefined && this.goal.isVisible === true) {
			// console.log(this.goal);
			this.goal.drawGoal(tileX, tileY);
		} else {
			backgroundContext.drawImage(this.texture, tileX, tileY, TILESIZE, TILESIZE);
		}

	 	backgroundContext.fillStyle = "black";
	 	if (this.wallTop) {
	 		context.fillRect(tileX, tileY, TILESIZE, WALLSIZE / 2);
	 	} if (this.wallBottom) {
	 		context.fillRect(tileX, tileY + TILESIZE - WALLSIZE / 2, TILESIZE, WALLSIZE / 2);
	 	} if (this.wallLeft) {
	 		context.fillRect(tileX, tileY, WALLSIZE / 2, TILESIZE);
	 	} if (this.wallRight) {
	 		context.fillRect(tileX + TILESIZE - WALLSIZE / 2, tileY, WALLSIZE / 2, TILESIZE);
	 	}
	};
};
