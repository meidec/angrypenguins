/**
 * Map holds all the tiles in a [rows][columns]-matrix.
 * Also draws the Map.
 * @param  {int} rows
 * @param  {int} columns
 */
var Map = function() {
	this.rows = 16;
	this.columns = 16;

	this.goalList = [];
	this.currentGoal = undefined;

	this.createMapTiles = function(paramMap) {

		var iceTexture = document.getElementById("iceTexture");

		this.tileMap = [];
		for (var i = 0; i < this.rows; i++) {
			this.tileMap[i] = [];
			for (var j = 0; j < this.columns; j++) {
				//// console.log(paramMap);
				this.tileMap[i][j] = new Tile(paramMap[i][j].wallTop, paramMap[i][j].wallBottom, paramMap[i][j].wallLeft, paramMap[i][j].wallRight);
				this.tileMap[i][j].texture = iceTexture;

				if (paramMap[i][j].goal !== undefined) {
					this.tileMap[i][j].goal = new Goal();
					this.goalList.push(this.tileMap[i][j].goal);
				}
			}
		}
	};

	/**
	 * Draws the whole map by going through the matrix
	 * and calling each tiles own draw().
	 *
	 */
	this.drawMap = function() {
		var x = TILESIZE,
			y = TILESIZE;

		backgroundContext.beginPath();
		

		for (var i = 0; i < this.rows; i++) {
			for (var j = 0; j < this.columns; j++) {
				// moved the drawmethod in every Tile
				this.tileMap[j][i].drawTile(x, y);
				x += TILESIZE;
			}
			x = TILESIZE;
			y += TILESIZE;

		}
		backgroundContext.stroke();
	};

	/**
	 * returns the tile's x / or y coordinate, where the first wall is found
	 * @param	{int} startX
	 * @param	{int} startY
	 * @param	{string} direction
	 */
	 this.getCollidingTile = function(startX, startY, direction) {
	 	var tmpTile, i;
	 	// UP
		if (direction == "up") {
			for (i = startY; i >= 0; i--) {
				tmpTile = this.tileMap[startX][i];

				if (tmpTile.hasPenguin && i != startY) {
					return i + 1;
				}
				if (tmpTile.wallTop) {
					return i;
				}
			}
		}
		// RIGHT
		else if (direction == "right") {
			for (i = startX; i < this.columns; i++) {
				tmpTile = this.tileMap[i][startY];
				if (tmpTile.hasPenguin && i != startX) {
					return i - 1;
				}
				if (tmpTile.wallRight) {
					return i;
				}
			}
		}
		// DOWN
		else if (direction == "down") {
			for (i = startY; i < this.rows; i++) {
				tmpTile = this.tileMap[startX][i];
				if (tmpTile.hasPenguin && i != startY) {
					return i - 1;
				}
				if (tmpTile.wallBottom) {
					return i;
				}
			}
		}
		// LEFT
		else if (direction == "left") {
			for (i = startX; i >= 0; i--) {
				tmpTile = this.tileMap[i][startY];
				if (tmpTile.hasPenguin && i != startX) {
					return i + 1;
				}
				if (tmpTile.wallLeft) {
					return i;
				}
			}
		}
	 };

	 this.newRandomGoal = function() {
	 	if (this.currentGoal !== undefined) {
	 		this.currentGoal.isVisible = false;
	 	}
	 	var randomNumb = Math.floor(Math.random() * this.goalList.length);
	 	this.currentGoal = this.goalList[randomNumb];
	 	this.goalList.splice(randomNumb, 1);
	 	this.currentGoal.isVisible = true;


	 	// console.log(this.goalList);
	};


	this.randomizeGoals = function() {

		var colorList = [],
			availableColors,
			randomColor,

			rainbowGoalTexture = document.getElementById('rainbowGoal'),
			blueGoalTexture = document.getElementById("blueGoal"),
			redGoalTexture = document.getElementById("redGoal"),
			yellowGoalTexture = document.getElementById("yellowGoal"),
			greenGoalTexture = document.getElementById("greenGoal");

		for (var i = 0; i < 4; i++) {
			colorList.push("red");
			colorList.push("blue");
			colorList.push("green");
			colorList.push("yellow");
		}
		colorList.push("rainbow");
		availableColors = colorList.length;
		for (i = 0; i < this.goalList.length; i++) {
			randomColor = Math.floor(Math.random() * availableColors);
			this.goalList[i].color = colorList[randomColor];
			if (this.goalList[i].color == "rainbow") {
				this.goalList[i].texture = rainbowGoalTexture;
			} else if (this.goalList[i].color == "blue") {
				this.goalList[i].texture = blueGoalTexture;
			}  else if (this.goalList[i].color == "red") {
				this.goalList[i].texture = redGoalTexture;
			}  else if (this.goalList[i].color == "yellow") {
				this.goalList[i].texture = yellowGoalTexture;
			}  else if (this.goalList[i].color == "green") {
				this.goalList[i].texture = greenGoalTexture;
			}
			colorList.splice(randomColor, 1);
			availableColors -= 1;
		}
	};

	this.clearPenguins = function() {
		for (var i = 0; i< this.columns; i++) {
			for (var j = 0; j < this.rows; j++) {
				this.tileMap[i][j].hasPenguin = false;
			}
		}
	};
};
