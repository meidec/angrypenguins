/**
 * Models a single Penguin on the map.
 * @param  {int} x
 * @param  {int} y
 * @param  {string} color
 */

var PENGUIN_MOVEMENT_SPEED = 0.2,
	ANIMATION_REFRESH_SPEED = 2,
	animationLoopId,
	isAnimationInProgress = false;


var Penguin = function(position, color, penguinTexture) {
	this.x = position[0];
	this.y = position[1];
	this.width = TILESIZE;
	this.height = TILESIZE;
	this.color = color;
	this.penguinTexture = penguinTexture;

	map.tileMap[this.x][this.y].hasPenguin = true;

	/**
	 * Draws a single penguin with the right color.
	 */
	this.drawPenguin = function() {
		penguinContext.fillStyle = this.color;
		// since penguin uses simpler coordinates, we need to transform them into pixels when drawing the penguin
		penguinContext.drawImage(this.penguinTexture, TILESIZE + this.x * TILESIZE, TILESIZE + this.y * TILESIZE, this.width, this.height);
	};

	/**
	 * Erases penguin from penguinCanvas so that it can be redrawn to another location
	 */
	this.erasePenguin = function() {
		penguinContext.clearRect(TILESIZE + this.x * TILESIZE, TILESIZE + this.y * TILESIZE, this.width, this.height);
	};

	/**
	 * Move method for penguin, direction is determined by param
	 * @params {string} direction
	 */
	this.movePenguin = function(direction) {
		// first clear the penguin from canvas
		//this.erasePenguin();
		if (!isAnimationInProgress) {
			newCoordinate = 0;
			// UP
			if (direction == "up") {
				newCoordinate = map.getCollidingTile(this.x, this.y, direction);
			}
			// RIGHT
			else if (direction == "right") {
				newCoordinate = map.getCollidingTile(this.x, this.y, direction);		
			}
			// DOWN
			else if (direction == "down") {
				newCoordinate = map.getCollidingTile(this.x, this.y, direction);
			}
			// LEFT
			else if (direction == "left") {
				newCoordinate = map.getCollidingTile(this.x, this.y, direction);
			}
			disableButtons(true, false);
			this.animateMovement(newCoordinate, direction);
			//this.drawPenguin();
		}
	};

	/**
	 * initiates animationloop and disables other functions such as changing current penguin and moving penguin
	 *
	 * @params {int} targetCoordinate
	 * @params {string} direction: up, right, down, left
	 */
	this.animateMovement = function(targetCoordinate, direction) {
		map.tileMap[this.x][this.y].hasPenguin = false;
		isAnimationInProgress = true;
		animationLoopId = 
		window.setInterval(function() {
			penguinAnimationLoop(currentPenguin, direction, targetCoordinate)
		}, ANIMATION_REFRESH_SPEED);
	};
};

/**
 * This is called in ANIMATION_REFRESH_SPEED ms intervals
 * moves penguin PENGUIN_MOVEMENT_SPEED amount towards the targetCoordinate
 * stops the animation loop when the targetCoordinate is reached
 *
 */
function penguinAnimationLoop(penguin, direction, targetCoordinate) {
	var distanceToTravel = 0;
	penguin.erasePenguin();
	// UP
	if (direction == "up") {
		distanceToTravel = penguin.y - targetCoordinate;

		if (distanceToTravel <= PENGUIN_MOVEMENT_SPEED) {
			penguin.y = targetCoordinate;
		} else {
			penguin.y -= PENGUIN_MOVEMENT_SPEED;
		}
	}
	// RIGHT
	else if (direction == "right") {	
		distanceToTravel = targetCoordinate - penguin.x;

		if (distanceToTravel <= PENGUIN_MOVEMENT_SPEED) {
			penguin.x = targetCoordinate;
		} else {
			penguin.x += PENGUIN_MOVEMENT_SPEED;
		}
	}
	// DOWN
	else if (direction == "down") {
		distanceToTravel = targetCoordinate - penguin.y;
	
		if (distanceToTravel <= PENGUIN_MOVEMENT_SPEED) {
			penguin.y = targetCoordinate;
		} else {
			penguin.y += PENGUIN_MOVEMENT_SPEED;
		}
	}
	// LEFT
	else if (direction == "left") {
		distanceToTravel = penguin.x - targetCoordinate;

		if (distanceToTravel <= PENGUIN_MOVEMENT_SPEED) {
			penguin.x = targetCoordinate;
		} else {
			penguin.x -= PENGUIN_MOVEMENT_SPEED;
		}
	}

	if (distanceToTravel <= PENGUIN_MOVEMENT_SPEED) {
		disableButtons(false);
		map.tileMap[penguin.x][penguin.y].hasPenguin = true;
		window.clearInterval(animationLoopId);
		isAnimationInProgress = false;
		checkWinner();
	}
	penguin.drawPenguin();
}
