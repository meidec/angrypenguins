/**
 * Models a single goal inside the Tile.
 * A new Goal() defaults to nonexistant -state.
 */
var Goal = function() {
    this.color = undefined;
    this.texture = undefined;
    this.isVisible = false;

    this.drawGoal = function(xPosition, yPosition) {
        backgroundContext.drawImage(this.texture, xPosition, yPosition, TILESIZE, TILESIZE);
    };
};
